<?php

require __DIR__ . '/vendor/autoload.php';

$calculator = new \App\Classes\SimpleComplexNumberCalculator();
$firstComplexNumber = new \App\Classes\SimpleComplexNumber(23,1);
$secondComplexNumber = new \App\Classes\SimpleComplexNumber(3,1);

echo PHP_EOL . "Калькулятор комплекскных числе приветствует вас!" . PHP_EOL;

echo  PHP_EOL . "Сложение:" . PHP_EOL;
echo $firstComplexNumber . "  +  " . $secondComplexNumber . "  =  " . $calculator->addition($firstComplexNumber,$secondComplexNumber) . PHP_EOL;

echo  PHP_EOL . "Вычитание:" . PHP_EOL;
echo $firstComplexNumber . "  -  " . $secondComplexNumber . "  =  " . $calculator->subtraction($firstComplexNumber,$secondComplexNumber) . PHP_EOL;

echo  PHP_EOL . "Умножение:" . PHP_EOL;
echo $firstComplexNumber . "  *  " . $secondComplexNumber . "  =  " . $calculator->multiplication($firstComplexNumber,$secondComplexNumber) . PHP_EOL;

echo  PHP_EOL . "Деление:" . PHP_EOL;
echo $firstComplexNumber . "  /  " . $secondComplexNumber . "  =  " . $calculator->division($firstComplexNumber,$secondComplexNumber) . PHP_EOL;

echo PHP_EOL;