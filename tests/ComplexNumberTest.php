<?php

namespace Tests;

use App\Classes\SimpleComplexNumber;
use PHPUnit\Framework\TestCase;

class ComplexNumberTest extends TestCase
{
    /**
     * @test
     * @dataProvider complexNumbers
     * @param $realPart
     * @param $imaginaryPart
     * @param $expected
     */

    public function it_returns_valid_number_when_converting_to_string($realPart, $imaginaryPart, $expected)
    {

        $number = new SimpleComplexNumber($realPart,$imaginaryPart);

        $this->assertEquals($expected, (string)$number);

    }

    public function complexNumbers()
    {
        return [
            'With two parts' => [
                'real_part'      => 5,
                'imaginary_part' => 3,
                'expected'       => '5 + 3i',
            ],
            'With real part only' => [
                'real_part'      => 7,
                'imaginary_part' => null,
                'expected'       => '7',
            ],
            'With imaginary part only' => [
                'real_part'      => 0,
                'imaginary_part' => 2,
                'expected'       => '2i',
            ],
            'With both empty parts' => [
                'real_part'      => null,
                'imaginary_part' => 0,
                'expected'       => '0',
            ],
            'With negative two parts' => [
                'real_part'      => -7,
                'imaginary_part' => -9,
                'expected'       => '-7 - 9i',
            ],
            'With negative real part only' => [
                'real_part'      => -17,
                'imaginary_part' => 2,
                'expected'       => '-17 + 2i',
            ],
            'With negative imaginary part only' => [
                'real_part'      => 10,
                'imaginary_part' => -2,
                'expected'       => '10 - 2i',
            ],
            'With negative real part and empty imaginary part' => [
                'real_part'      => -1,
                'imaginary_part' => 0,
                'expected'       => '-1',
            ],
            'With negative imaginary part and empty real part' => [
                'real_part'      => 0,
                'imaginary_part' => -8,
                'expected'       => '-8i',
            ],
            'With imaginary part only and equals one' => [
                'real_part'      => 0,
                'imaginary_part' => 1,
                'expected'       => 'i',
            ],
            'With imaginary part equals one' => [
                'real_part'      => 7,
                'imaginary_part' => 1,
                'expected'       => '7 + i',
            ],
            'With imaginary part only and equals minus one' => [
                'real_part'      => 0,
                'imaginary_part' => -1,
                'expected'       => '-i',
            ],
            'With imaginary part equals minus one' => [
                'real_part'      => 2,
                'imaginary_part' => -1,
                'expected'       => '2 - i',
            ],


        ];
    }
}