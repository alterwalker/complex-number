<?php

namespace Tests;

use App\Classes\SimpleComplexNumber;
use App\Classes\SimpleComplexNumberCalculator;
use PHPUnit\Framework\TestCase;

class ComplexNumberCalculatorTest extends TestCase
{
    /**
     * @test
     * @dataProvider complexNumbersForAddition
     * @param $first
     * @param $second
     * @param $expected
     */

    public function it_calculates_valid_additional($first, $second, $expected)
    {

        $calculator = new SimpleComplexNumberCalculator;

        $result = $calculator->addition($first,$second);

        $this->assertEquals($expected, (string)$result);

    }

    public function complexNumbersForAddition()
    {
        return [
            'With dataset #1' => [
                'first'    => new SimpleComplexNumber(3,5),
                'second'   => new SimpleComplexNumber(2,3),
                'expected' => '5 + 8i',
            ],
            'With dataset #2' => [
                'first'    => new SimpleComplexNumber(0,9),
                'second'   => new SimpleComplexNumber(null,13),
                'expected' => '22i',
            ],
            'With dataset #3' => [
                'first'    => new SimpleComplexNumber(7,9),
                'second'   => new SimpleComplexNumber(3,-9),
                'expected' => '10',
            ],
            'With dataset #4' => [
                'first'    => new SimpleComplexNumber(7,null),
                'second'   => new SimpleComplexNumber(-10,0),
                'expected' => '-3',
            ],
        ];
    }

    /**
     * @test
     * @dataProvider complexNumbersForSubtraction
     * @param $first
     * @param $second
     * @param $expected
     */

    public function it_calculates_valid_subtraction($first, $second, $expected)
    {

        $calculator = new SimpleComplexNumberCalculator;

        $result = $calculator->subtraction($first,$second);

        $this->assertEquals($expected, (string)$result);

    }

    public function complexNumbersForSubtraction()
    {
        return [
            'With dataset #1' => [
                'first'    => new SimpleComplexNumber(3,5),
                'second'   => new SimpleComplexNumber(2,3),
                'expected' => '1 + 2i',
            ],
            'With dataset #2' => [
                'first'    => new SimpleComplexNumber(0,9),
                'second'   => new SimpleComplexNumber(null,13),
                'expected' => '-4i',
            ],
            'With dataset #3' => [
                'first'    => new SimpleComplexNumber(7,9),
                'second'   => new SimpleComplexNumber(3,-9),
                'expected' => '4 + 18i',
            ],
            'With dataset #4' => [
                'first'    => new SimpleComplexNumber(7,null),
                'second'   => new SimpleComplexNumber(-10,0),
                'expected' => '17',
            ],
        ];
    }

    /**
     * @test
     * @dataProvider complexNumbersForMultiplication
     * @param $first
     * @param $second
     * @param $expected
     */

    public function it_calculates_valid_multiplication($first, $second, $expected)
    {

        $calculator = new SimpleComplexNumberCalculator;

        $result = $calculator->multiplication($first,$second);

        $this->assertEquals($expected, (string)$result);

    }

    public function complexNumbersForMultiplication()
    {
        return [
            'With dataset #1' => [
                'first'    => new SimpleComplexNumber(1,-1),
                'second'   => new SimpleComplexNumber(3,6),
                'expected' => '9 + 3i',
            ],
            'With dataset #2' => [
                'first'    => new SimpleComplexNumber(2,3),
                'second'   => new SimpleComplexNumber(-1,1),
                'expected' => '-5 - i',
            ],
        ];
    }

    /**
     * @test
     * @dataProvider complexNumbersForDivision
     * @param $first
     * @param $second
     * @param $expected
     */

    public function it_calculates_valid_division($first, $second, $expected)
    {

        $calculator = new SimpleComplexNumberCalculator;

        $result = $calculator->division($first,$second);

        $this->assertEquals($expected, (string)$result);

    }

    public function complexNumbersForDivision()
    {
        return [
            'With dataset #1' => [
                'first'    => new SimpleComplexNumber(13,1),
                'second'   => new SimpleComplexNumber(7,-6),
                'expected' => '1 + i',
            ],
            'With dataset #2' => [
                'first'    => new SimpleComplexNumber(3,4),
                'second'   => new SimpleComplexNumber(2,1),
                'expected' => '2 + i',
            ],
            'With dataset #3' => [
                'first'    => new SimpleComplexNumber(23,1),
                'second'   => new SimpleComplexNumber(3,1),
                'expected' => '7 - 2i',
            ],
        ];
    }
}