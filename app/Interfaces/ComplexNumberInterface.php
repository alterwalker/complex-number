<?php

namespace App\Interfaces;

interface ComplexNumberInterface
{
    public function getRealPart();
    public function setRealPart($realPart);
    public function getImaginaryPart();
    public function setImaginaryPart($imaginaryPart);
    public function __toString();
}