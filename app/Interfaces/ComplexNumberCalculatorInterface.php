<?php

namespace App\Interfaces;

interface ComplexNumberCalculatorInterface
{
   public function addition(ComplexNumberInterface $first, ComplexNumberInterface $second) : ComplexNumberInterface;
   public function subtraction(ComplexNumberInterface $first, ComplexNumberInterface $second) : ComplexNumberInterface;
   public function multiplication(ComplexNumberInterface $first, ComplexNumberInterface $second) : ComplexNumberInterface;
   public function division(ComplexNumberInterface $first, ComplexNumberInterface $second) : ComplexNumberInterface;
}