<?php

namespace App\Classes;

use App\Interfaces\ComplexNumberCalculatorInterface;
use App\Interfaces\ComplexNumberInterface;
use phpDocumentor\Reflection\Types\This;

class SimpleComplexNumberCalculator implements ComplexNumberCalculatorInterface
{

    /**
     * @param ComplexNumberInterface $first
     * @param ComplexNumberInterface $second
     * @return ComplexNumberInterface
     */
    public function addition(ComplexNumberInterface $first, ComplexNumberInterface $second) : ComplexNumberInterface
    {
        $result = new SimpleComplexNumber();

        $result->setRealPart($first->getRealPart() + $second->getRealPart());
        $result->setImaginaryPart($first->getImaginaryPart() + $second->getImaginaryPart());

        return $result;
    }

    /**
     * @param ComplexNumberInterface $first
     * @param ComplexNumberInterface $second
     * @return ComplexNumberInterface
     */
    public function subtraction(ComplexNumberInterface $first, ComplexNumberInterface $second) : ComplexNumberInterface
    {
        $result = new SimpleComplexNumber();

        $result->setRealPart($first->getRealPart() - $second->getRealPart());
        $result->setImaginaryPart($first->getImaginaryPart() - $second->getImaginaryPart());

        return $result;
    }

    /**
     * @param ComplexNumberInterface $first
     * @param ComplexNumberInterface $second
     * @return ComplexNumberInterface
     */
    public function multiplication(ComplexNumberInterface $first, ComplexNumberInterface $second) : ComplexNumberInterface
    {
        $result = new SimpleComplexNumber();

        $a1 = $first->getRealPart();
        $a2 = $second->getRealPart();
        $b1 = $first->getImaginaryPart();
        $b2 = $second->getImaginaryPart();

        $result->setRealPart($a1*$a2 - $b1*$b2);
        $result->setImaginaryPart($a1*$b2 + $b1*$a2);
        return $result;
    }

    /**
     * @param ComplexNumberInterface $first
     * @param ComplexNumberInterface $second
     * @return ComplexNumberInterface
     */
    public function division(ComplexNumberInterface $first, ComplexNumberInterface $second) : ComplexNumberInterface
    {
        $result = new SimpleComplexNumber();

        $conjugateComplexNumber = $this->getConjugateComplexNumber($second);

        $numerator = $this->multiplication($first,$conjugateComplexNumber);

        $denominator = $second->getRealPart()**2 + $second->getImaginaryPart()**2;

        $result->setRealPart($numerator->getRealPart() / $denominator);
        $result->setImaginaryPart($numerator->getImaginaryPart() / $denominator);

        return $result;

    }

    /**
     * @param ComplexNumberInterface $number
     * @return ComplexNumberInterface
     */
    protected function getConjugateComplexNumber(ComplexNumberInterface $number) : ComplexNumberInterface
    {
        $result = new SimpleComplexNumber();
        $result->setRealPart($number->getRealPart());
        $result->setImaginaryPart(-1 * $number->getImaginaryPart());

        return $result;
    }

}