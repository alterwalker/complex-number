<?php

namespace App\Classes;

use App\Interfaces\ComplexNumberInterface;

class SimpleComplexNumber implements ComplexNumberInterface
{
    private $realPart;
    private $imaginaryPart;

    public function __construct( $realPart = 0, $imaginaryPart = 0)
    {
        $this->realPart = $realPart;
        $this->imaginaryPart = $imaginaryPart;
    }

    /**
     * @return mixed
     */
    public function getRealPart()
    {
        return $this->realPart;
    }

    /**
     * @param mixed $realPart
     */
    public function setRealPart($realPart)
    {
        $this->realPart = $realPart;
    }

    /**
     * @return mixed
     */
    public function getImaginaryPart()
    {
        return $this->imaginaryPart;
    }

    /**
     * @param mixed $imaginaryPart
     */
    public function setImaginaryPart($imaginaryPart)
    {
        $this->imaginaryPart = $imaginaryPart;
    }

    public function __toString()
    {
        $result = (string) 0;
        // убираем null и пустые строки, заменяя их на 0
        $this->realPart = empty($this->realPart) ? 0 : $this->realPart;
        $this->imaginaryPart = empty($this->imaginaryPart) ? 0 : $this->imaginaryPart;

        if(!empty($this->realPart) && !empty($this->imaginaryPart)) {
            if(abs($this->imaginaryPart) == 1) {
                if($this->imaginaryPart < 0) {
                    $result = $this->realPart . ' - i';
                } else {
                    $result = $this->realPart . ' + i';
                }
            } else {
                if($this->imaginaryPart < 0) {
                    $result = $this->realPart . ' - ' . abs($this->imaginaryPart) . 'i';
                } else {
                    $result = $this->realPart . ' + ' . $this->imaginaryPart . 'i';
                }
            }


        } else if(!empty($this->imaginaryPart)) {
            if(abs($this->imaginaryPart) == 1) {
                if($this->imaginaryPart > 0 ) {
                    $result = 'i';
                } else {
                    $result = '-i';
                }

            } else {
                $result = $this->imaginaryPart . 'i';
            }
        } else if(!empty($this->realPart)) {
            $result = (string) $this->realPart;
        }

        return $result;
    }

}